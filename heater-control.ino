/*
 * Plug + of relay into pin 13.
 * Plug - of relay into ground.
 * Plug - (black) end of temp probe into ground.
 * Plug + end of Resistor to +5v.
 * Plug - end of resistor and + (red) wire of temp probe into A0.
 */
#include <Relay.h>


int relayPin = 13;
int relayTimePeriod = 1; // in seconds
double output;

// Create Relay object
Relay relay(relayPin, relayTimePeriod);


void setup() {
  analogReadResolution(12);
  pinMode(A1, INPUT);
  relay.setRelayMode(relayModeAutomatic);
}


void loop() {
  readThermometer();
  relay.setDutyCyclePercent(output);
  relay.loop();
}


void readThermometer() {
  int sensorVal = analogRead(A1);
  output = map(sensorVal, 0, 4095, 100, 0);
  output = output / 100.0;
}
